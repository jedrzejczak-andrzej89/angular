/**
 * Created by aj on 28.02.16.
 */

'use strict';

// TODO: service should return data, not promise

searchApp.controller('JobIndexController', function($scope, JobService, JobService1) {

    $scope.searchParameters = {};
    $scope.search = '';

    getJobs();
    getFilters();

    function getJobs() {
        JobService1.query($scope.searchParameters).then(function(response){
            $scope.jobs = response.data;
        });
    }

    function  getFilters() {
        JobService1.filters().then(function(response){
            $scope.filterSections = response.data;
        });
    }

    $scope.$watch('search', function (newVal, oldVal) {
        console.log($scope.search);
    });

    $scope.$watchCollection('searchParameters', function (newVal, oldVal) {
        console.log($scope.searchParameters);
        console.log('xx');
        //alert('x');
    });

    //$scope.jobs = Job.query();
    //$scope.filterSections = Job.filters();
});
