/**
 * Created by aj on 28.02.16.
 */

'use strict';


searchApp.factory('JobService', function($resource) {
    return $resource('/api/jobs/:method:id', {
        id: '@id',
        method: '@method'
    }, {
        filters: {
            method: 'GET',
            params: {
                method: 'filters'
            }
        }
    });
});

searchApp.factory('JobService1', function($http) {
    var path = 'api/jobs';

    return {
        query: function(parameters) {
            return $http.get(path + '?' + $.param(parameters));
        },
        filters: function() {
            return $http.get(path + '/filters');
        }
    };

    /*service.filters = function() {
     return $http.get(path + '/filters');
     };

     /!*service.fetch = function (itemId) {
     return $http.get(getUrlForId(itemId));
     };

     service.create = function (item) {
     return $http.post(getUrl(), item);
     };

     service.update = function (itemId, item) {
     return $http.put(getUrlForId(itemId), item);
     };

     service.destroy = function (itemId) {
     return $http.delete(getUrlForId(itemId));
     };*/
});
