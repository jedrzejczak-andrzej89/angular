<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Page Description">
        <meta name="author" content="aj">
        <title>Page Title</title>

        <!-- Bootstrap -->
        <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="/bower_components/angular/angular.min.js"></script>
        <script src="/bower_components/angular-resource/angular-resource.min.js"></script>

        <script src="/js/app.js"></script>
        <script src="/js/services/job.js"></script>
        <script src="/js/controllers/job/index.js"></script>
    </head>
    <body>
    <div class="container" ng-app="searchApp">
        <h2>Jobs offer search</h2>
        <div class="row" ng-controller="JobIndexController">
            <div class="col-md-4">
                <div class="list-group">
                    <input type="text" ng-model="search" value="">
                    <div class="list-group-item" ng-repeat="filterSection in filterSections">
                        <h4 class="list-group-item-heading">{{ filterSection.title }}</h4>
                        <div class="checkbox" ng-repeat="option in filterSection.options">
                            <label>
                                <input type="checkbox" ng-model="searchParameters[filterSection.key][option.key]" name="searchParameters[{{filterSection.key}}][]" value="{{ option.key }}"> {{ option.value }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="list-group">
                    <a href="#" class="list-group-item" ng-repeat="job in jobs">
                        <small>{{ job.update_date }}</small>
                        <h4 class="list-group-item-heading">{{ job.title }}</h4>
                        <hr>
                        <p class="list-group-item-text">
                            Regions:
                            <span class="badge" ng-repeat="region in job.regions">
                                {{ region.name }}, {{ region.cities }}
                            </span>
                        </p>
                        <p class="list-group-item-text">
                            Zatrudnieni: ???
                        </p>

                    </a>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>