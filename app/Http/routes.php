<?php
Blade::setContentTags('<%', '%>');        // for variables and all things Blade
Blade::setEscapedContentTags('<%%', '%%>');   // for escaped data

Route::get('/', function () {
    return view('search');
});


Route::get('/api/jobs', function () {
    return [
        [
            'title' => 'Job1',
        ],
        [
            'title' => 'Job2',
        ],
    ];
});

Route::get('/api/jobs/filters', function () {
    return [
        [
            'key' => 'offersOfThePeriod',
            'title' => 'Oferty z okresu',
            'options' => [
                [
                    'key' => '',
                    'value' => 'wszystkie',
                ],
                [
                    'key' => '1d',
                    'value' => '24h',
                ],
                [
                    'key' => '3d',
                    'value' => '3 dni',
                ],
                [
                    'key' => '7d',
                    'value' => '7 dni',
                ],
                [
                    'key' => '14d',
                    'value' => '14 dni',
                ],
                [
                    'key' => '30d',
                    'value' => '30 dni',
                ],
            ]
        ]
    ];
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
